class Estadistica:
    def calcular_estadistica(cadena):
        arregloEntrada = cadena.split(",")
        arreglo = [-1,-1,-1,-1]

        if cadena == "":
            arreglo[0] = 0
            arreglo[1] = 0
            arreglo[2] = 0
            arreglo[3] = 0
        elif "," in cadena:
            arregloEntrada = list(map(int, arregloEntrada))
            arreglo[0] = len(arregloEntrada)
            arreglo[1] = min(arregloEntrada)
            arreglo[2] = max(arregloEntrada)
            arreglo[3] = Estadistica.promedio(arregloEntrada)
            #arreglo[3] = int((arregloEntrada[0]+arregloEntrada[1])/2)
        else:
            arreglo[0] = 1
            arreglo[1] = int (cadena)
            arreglo[2] = int(cadena)
            arreglo[3] = int(cadena)

        return arreglo

    def promedio(lista):
        return sum(lista)/len(lista)