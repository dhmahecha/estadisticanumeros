from unittest import TestCase
import Estadistica

class EstadisticaTest(TestCase):
    def test_calcular_estadistica(self):
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("")[0], 0, "Cadena Vacia")
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("")[1], 0, "Cadena Vacia minimo.")
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("")[2], 0, "Cadena Vacia maximo.")
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("")[3], 0, "Cadena Vacia promedio.")
    def test_calcular_estadistica_unnumero(self):
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("1")[0], 1, "un número")
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("2")[1], 2, "un número minimo")
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("2")[2], 2, "un número maximo")
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("2")[3], 2, "un número promedio")
    def test_calcular_estadistica_dosnumeros(self):
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("1,5")[0], 2, "dos números.")
        self.assert_(Estadistica.Estadistica.calcular_estadistica("1,5")[1] == 1, "dos números y el minimo.")
        self.assert_(Estadistica.Estadistica.calcular_estadistica("1,5")[2] == 5, "dos números y el maximo")
        self.assert_(Estadistica.Estadistica.calcular_estadistica("1,5")[3] == 3, "dos números y el promedio")
    def test_calcular_estadistica_variosnumeros(self):
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("1,5,6,4")[0], 4, "varios números.")
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("5,6,4,1")[1], 1, "varios números y el minimo.")
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("5,4,1,6")[2], 6, "varios números y el maximo.")
        self.assertEqual(Estadistica.Estadistica.calcular_estadistica("5,4,1,6")[3], 4, "varios números y el promedio.")

